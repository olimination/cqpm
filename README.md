# CQ Process Manager

This small deamon Java application monitors defined CQ processes in the config.xml according to process failures.
If some runtime failures occur then it executes the defined actions like stop/start CQ, email notification.

**Technology stack**

- Spring 3.2.x
- SLF4J
- Launch4j
- Maven 3


## Building and Using the project

** Prepare Eclipse project files**

`root/> mvn eclipse:eclipse`

**Build and test**

`root/> mvn clean install`

**Run app directly in Maven**

`root/> mvn exec:java -Dexec.mainClass="org.vbs.cqpm.Main"`

or use batch script:

`root/> run-app.bat`

**Run app with java -jar command**

Extract the distribution zip and execute the jar file:

`java -jar jarfile` 

**Run exe-file from distribution zip**

- Unpack *-dist.zip file
- Run cqpm.exe from commandline