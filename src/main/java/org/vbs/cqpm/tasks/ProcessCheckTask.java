package org.vbs.cqpm.tasks;

import java.util.concurrent.ScheduledFuture;

import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;

/**
 * The {@link ProcessCheckTask} interface defines a task for checking one
 * process. It contains a <code>check()</code> method which starts with the
 * whole check procedure. Mostly this {@link ProcessCheckTask#check()} is called
 * by the {@link Runnable#run()} method.
 * <p>
 * If the process failure checks really fail, then it executes the process
 * actions. These actions should be executed in sequence to prevent concurrency
 * issues with the process. Because if the run/check method has finished the
 * Spring Scheduler will start again a new scheduled task which will again
 * executes these actions. That's why its important to make sure that all
 * operations are finished before a new Scheduler task execution.
 * <p>
 * A {@link ProcessCheckTask} instance should also make sure that it starts the
 * ProcessFailurePattern checks, but also stops them again nicely. The
 * ProcessFailurePattern objects may be got by
 * {@link Process#getFailurePatternList()}.
 * 
 * @author oliver.burkhalter
 * @see {@link Process}, {@link ProcessFailurePattern}
 */
public interface ProcessCheckTask extends Runnable {

    void check();

    void cancel();

    Process getProcess();

    void setScheduledFuture(ScheduledFuture<?> future);

    ScheduledFuture<?> getScheduledFuture();

    /**
     * Defines the amount of time in milliseconds for waiting per iteration
     * check. The max. timeout time is calculated like this: timeoutInterval *
     * timeoutCount.
     * 
     * @param timeoutIntervalTimeMillis
     */
    void setTimeoutIntervalTimeMillis(int timeoutIntervalTimeMillis);

    /**
     * Defines the max. of iterations of checks for the process failures.
     * 
     * @param maxTimeoutCount
     */
    void setMaxTimeoutCount(int maxTimeoutCount);

    /**
     * @param cron
     *            The cron expression as string.
     */
    void setCron(String cron);

    String getCron();

}
