package org.vbs.cqpm.tasks.impl;

import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;
import org.vbs.cqpm.tasks.ProcessCheckTask;

/**
 * Default implementation of the {@link ProcessCheckTask} interface.
 * <p>
 * It executes the process failure checks from the given configured process and
 * monitors the duration of these checks. If the reach the timeout time then it
 * stops the check threads and cancels the scheduled task.
 * <p>
 * If a scheduled task get cancelled, then you currently only can re-schedule
 * the task with a restart of the application.
 * 
 * @author oliver.burkhalter
 */
public class ProcessCheckTaskImpl implements ProcessCheckTask {

    private static final Logger logger = LoggerFactory.getLogger(ProcessCheckTaskImpl.class);

    /**
     * Defines the cron expression for this task execution.
     */
    private String cron = "*/10 * * * * *";

    private int timeoutIntervalTimeMillis = 20000;
    private int maxTimeoutCount = 6;
    private int currentTimeoutCount = 0;
    private Process process;
    private ScheduledFuture<?> scheduledFuture = null;

    public void run() {
	check();
    }

    public void check() {

	startAllProcessFailureChecks();

	// Continuously checking if the process failure checks have finished or
	// the timeout count is reached
	while (!allChecksFinished() && currentTimeoutCount <= maxTimeoutCount) {
	    try {
		logger.debug("Wait for next process check interval, id=" + this);
		Thread.sleep(timeoutIntervalTimeMillis);
	    } catch (InterruptedException e) {
		logger.error("ProcessCheckTask has been interrupted: " + process.getName());
	    }
	    currentTimeoutCount++;
	}

	// Timeout reached?
	if (currentTimeoutCount >= maxTimeoutCount) {
	    logger.warn("Timeout reached for ProcessCheckTask: " + process.getName());
	    cancel();
	    // TODO Mail action
	}

	stopAllProcessFailureChecks();

	// Reset timeout counter
	currentTimeoutCount = 0;

	if (process.failed()) {
	    logger.warn("Process has failed: " + process.getName());
	    process.executeAllActions();
	}
    }

    private void startAllProcessFailureChecks() {
	for (ProcessFailurePattern failurePattern : process.getFailurePatternList()) {
	    failurePattern.start(process);
	}
    }

    private void stopAllProcessFailureChecks() {
	for (ProcessFailurePattern failurePattern : process.getFailurePatternList()) {
	    failurePattern.stop();
	}
    }

    private boolean allChecksFinished() {
	int allFinished = 0;

	for (ProcessFailurePattern failurePattern : process.getFailurePatternList()) {
	    allFinished = failurePattern.finished() ? allFinished + 1 : allFinished;
	}

	return allFinished == process.getFailurePatternList().size();
    }

    public void cancel() {
	if (scheduledFuture != null) {
	    scheduledFuture.cancel(true);
	}
    }

    public void setProcess(Process process) {
	this.process = process;
    }

    public Process getProcess() {
	return process;
    }

    public void setScheduledFuture(ScheduledFuture<?> scheduledFuture) {
	this.scheduledFuture = scheduledFuture;
    }

    public ScheduledFuture<?> getScheduledFuture() {
	return scheduledFuture;
    }

    public void setTimeoutIntervalTimeMillis(int timeoutIntervalTimeMillis) {
	this.timeoutIntervalTimeMillis = timeoutIntervalTimeMillis;
    }

    public void setMaxTimeoutCount(int maxTimeoutCount) {
	this.maxTimeoutCount = maxTimeoutCount;
    }

    public String getCron() {
	return cron;
    }

    public void setCron(String cron) {
	this.cron = cron;
    }
}
