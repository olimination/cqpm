package org.vbs.cqpm.core;

/**
 * A {@link ProcessFailurePattern} object executes checks for the process and
 * sets the process status (failed=true/false).
 * <p>
 * The {@link ProcessFailurePattern#start(Process)} method may be executed
 * sequentially or concurrently.
 * 
 * @author oliver.burkhalter
 * @see {@link Process#failed()}
 */
public interface ProcessFailurePattern {

    String getName();

    void start(Process process);

    void stop();

    /**
     * Checks if the process failure check has finished already. It may be that
     * some sub-threads are still in progress, so it returns false. If the
     * {@link ProcessFailurePattern} could finalize all the check threads, then
     * it returns true.
     * 
     * @return Returns true if the process failure check has finished, else
     *         false.
     */
    boolean finished();

}
