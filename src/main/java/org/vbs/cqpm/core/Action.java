package org.vbs.cqpm.core;

/**
 * The {@link Action} defines an action which is executed after a process
 * failure check.
 * <p>
 * Actions should not be executed in parallel otherwise there may be concurrency
 * issues because the scheduler re-executed the process check task again
 * although an action for the same process is still in progress, e.g.
 * starting/stopping CQ process.
 * 
 * @author oliver.burkhalter
 */
public interface Action {

    /**
     * @return Returns 0 if execution has finished without any errors, else
     *         something smaller/greater than 0.
     */
    int execute();
}
