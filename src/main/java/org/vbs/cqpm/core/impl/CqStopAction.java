package org.vbs.cqpm.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vbs.cqpm.core.Action;

public class CqStopAction implements Action {

    private static final Logger logger = LoggerFactory.getLogger(CqStopAction.class);

    public int execute() {
	// TODO Stop CQ through WMI or tasklist commandline
	logger.debug("Stopping CQ server...");
	return 0;
    }

}
