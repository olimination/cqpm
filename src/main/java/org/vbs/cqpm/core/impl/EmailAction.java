package org.vbs.cqpm.core.impl;

import org.vbs.cqpm.core.Action;

/**
 * The {@link EmailAction} class prepares and sends an email. The SMTP
 * configurations may be set through Spring's container.
 * 
 * @author oliver.burkhalter
 */
public class EmailAction implements Action {

    private String smtpHost;

    public int execute() {
	// TODO Send email
	return 0;
    }

    public String getSmtpHost() {
	return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
	this.smtpHost = smtpHost;
    }
}
