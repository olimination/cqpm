package org.vbs.cqpm.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;

/**
 * The {@link NonExistingProcessFailure} class checks if a given process object
 * is active and running.
 * 
 * @author oliver.burkhalter
 */
public class NonExistingProcessFailure implements ProcessFailurePattern, Runnable {

    private static final Logger logger = LoggerFactory.getLogger(NonExistingProcessFailure.class);

    private boolean finished;

    private Thread checkThread;
    private Process process;

    public String getName() {
	return this.getClass().getName();
    }

    public void run() {
	try {
	    logger.debug("Start NonExistingProcessFailure check, id=" + this);

	    // TODO Check existance of process: use WMI or tasklist commandline
	    Thread.sleep(5000);

	    // TODO Set then correct status
	    if ("test_java.exe".equals(process.getName())) {
		process.setFailed(true);
	    } else {
		process.setFailed(false);
	    }

	    finished = true;
	    logger.debug("NonExistingProcessFailure check has finished, id=" + this);
	} catch (InterruptedException e) {
	    logger.warn("NonExistingProcessFailure check has been interrupted: " + e.getMessage());
	}
    }

    public void start(Process process) {
	this.process = process;
	this.process.setFailed(false);
	finished = false;
	checkThread = new Thread(this);
	checkThread.start();
    }

    public void stop() {
	checkThread.interrupt();
    }

    public boolean finished() {
	return finished;
    }
}
