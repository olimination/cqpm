package org.vbs.cqpm.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;

/**
 * The {@link ThirteenPercentCpuLoadProcessFailure} checks over a longer period
 * if the specific CQ process hangs around 13% CPU load.
 * 
 * @author oliver.burkhalter
 */
public class ThirteenPercentCpuLoadProcessFailure implements ProcessFailurePattern, Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ThirteenPercentCpuLoadProcessFailure.class);

    private boolean finished;

    private Thread checkThread;
    private Process process;

    public String getName() {
	return this.getClass().getName();
    }

    public void run() {
	try {
	    logger.debug("Start ThirteenPercentCpuLoadProcessFailure check, id=" + this);

	    // TODO Check loop for 13% CPU load problem
	    Thread.sleep(5000);

	    // TODO Set then correct status
	    process.setFailed(true);

	    finished = true;
	    logger.debug("ThirteenPercentCpuLoadProcessFailure check has finished, id=" + this);
	} catch (InterruptedException e) {
	    logger.warn("ThirteenPercentCpuLoadProcessFailure check has been interrupted: " + e.getMessage());
	}
    }

    public void start(Process process) {
	this.process = process;
	this.process.setFailed(false);
	finished = false;
	checkThread = new Thread(this);
	checkThread.start();
    }

    public void stop() {
	checkThread.interrupt();
    }

    public boolean finished() {
	return finished;
    }

}
