package org.vbs.cqpm.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;

/**
 * The {@link CqRestartRequestProcessFailure} checks against a configured SFTP
 * server and looks for a "restart" file. If this restart file is available then
 * a CQ restart should be executed.
 * 
 * @author oliver.burkhalter
 */
public class CqRestartRequestProcessFailure implements Runnable, ProcessFailurePattern {

    private static final Logger logger = LoggerFactory.getLogger(CqRestartRequestProcessFailure.class);

    private boolean finished;

    private String sftpHost;

    private Thread checkThread;
    private Process process;

    public String getName() {
	return this.getClass().getName();
    }

    public void run() {
	try {
	    logger.debug("Start CqRestartRequestProcessFailure check, id=" + this);

	    // TODO Check SFTP server for restart file
	    Thread.sleep(5000);

	    // TODO Set then correct status
	    process.setFailed(true);

	    finished = true;
	    logger.debug("CqRestartRequestProcessFailure check has finished, id=" + this);
	} catch (InterruptedException e) {
	    logger.warn("CqRestartRequestProcessFailure check has been interrupted: " + e.getMessage());
	}
    }

    public void start(Process process) {
	this.process = process;
	this.process.setFailed(false);
	finished = false;
	checkThread = new Thread(this);
	checkThread.start();
    }

    public void stop() {
	checkThread.interrupt();
    }

    public boolean finished() {
	return finished;
    }

    public String getSftpHost() {
	return sftpHost;
    }

    public void setSftpHost(String sftpHost) {
	this.sftpHost = sftpHost;
    }
}
