package org.vbs.cqpm.core.impl;

import java.util.List;

import org.vbs.cqpm.core.Action;
import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;

/**
 * Default implementation of the {@link Process} interface.
 * 
 * @author oliver.burkhalter
 */
public class ProcessImpl implements Process {

    private boolean failed;
    private String name;
    private String path;
    private List<ProcessFailurePattern> failurePatternList;
    private List<Action> afterFailActionList;

    public void setName(String name) {
	this.name = name;
    }

    public String getName() {
	return name;
    }

    public void setPath(String path) {
	this.path = path;
    }

    public String getPath() {
	return path;
    }

    public void setFailurePatternList(List<ProcessFailurePattern> failurePatternList) {
	this.failurePatternList = failurePatternList;
    }

    public List<ProcessFailurePattern> getFailurePatternList() {
	return failurePatternList;
    }

    public void setAfterFailActionList(List<Action> afterFailActionList) {
	this.afterFailActionList = afterFailActionList;
    }

    public List<Action> getAfterFailActionList() {
	return afterFailActionList;
    }

    public boolean failed() {
	return failed;
    }

    public void setFailed(boolean failed) {
	this.failed = failed;
    }

    public void executeAllActions() {
	for(Action action : afterFailActionList) {
	    action.execute();
	}
    }
}
