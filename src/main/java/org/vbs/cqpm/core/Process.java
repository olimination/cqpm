package org.vbs.cqpm.core;

import java.util.List;

/**
 * Models a simple OS process with a name and the path to the executable.
 * <p>
 * It aggrates a list of process failure pattern checks and a list of actions
 * which should be executed after a process failure check.
 * 
 * @author oliver.burkhalter
 * @see {@link Process}, {@link Action}
 */
public interface Process {

    String getName();

    String getPath();

    List<ProcessFailurePattern> getFailurePatternList();

    List<Action> getAfterFailActionList();
    
    void executeAllActions();

    /**
     * This field is set by the {@link ProcessFailurePattern} object if there is
     * a process failure.
     * 
     * @return Returns true if there is a process failure, else false.
     */
    boolean failed();

    void setFailed(boolean failed);
}
