package org.vbs.cqpm;

import java.util.ArrayList;
import java.util.concurrent.ScheduledFuture;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.vbs.cqpm.tasks.ProcessCheckTask;

/**
 * Main class for the CQ Process Manager application.
 * <p>
 * All the configuration may be written in the <code>config.xml</code> file
 * which represents a proper Spring configuration file and is located in the
 * same directory of the jar/exe execution.
 * <p>
 * The configuration is straightforward because you simply can use Spring's
 * default configuration possibilites.
 * <p>
 * It initializes the process check task list and schedules the tasks to the
 * Scheduler. It also sets the returned {@link ScheduledFuture} object for the
 * process check task. This enables the application to control the execution of
 * the scheduled tasks. For example: The tasks should be stopped and the an
 * email notification should be send, if a process check task takes very long to
 * execute their process failure checks.
 * 
 * @author oliver.burkhalter
 * @since 20130524
 */
public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

	// Load log4j from external config file
	// This file may be located in the same jar/exe directory
	PropertyConfigurator.configure("log4j.properties");

	logger.debug("Starting app in: " + System.getProperty("user.dir"));

	// Load Spring context
	AbstractApplicationContext context = new ClassPathXmlApplicationContext("app-context.xml");
	ThreadPoolTaskScheduler scheduler = (ThreadPoolTaskScheduler) context.getBean("pmScheduler");

	@SuppressWarnings("unchecked")
	ArrayList<ProcessCheckTask> processCheckTaskList = (ArrayList<ProcessCheckTask>) context
		.getBean("processCheckTaskList");

	// Schedule the process check tasks
	for (ProcessCheckTask task : processCheckTaskList) {
	    ScheduledFuture<?> future = scheduler.schedule(task, new CronTrigger(task.getCron()));
	    task.setScheduledFuture(future);
	}

	while (!Thread.interrupted()) {
	    try {
		Thread.yield();
		Thread.sleep(2000);
	    } catch (InterruptedException e) {
		logger.error("Thread error: " + e.getMessage());
	    }
	}

	logger.debug("Stopping app...");

	scheduler.shutdown();
	context.close();

	logger.debug("Application stopped.");

	System.exit(0);
    }
}