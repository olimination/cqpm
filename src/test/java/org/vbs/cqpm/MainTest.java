package org.vbs.cqpm;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.vbs.cqpm.tasks.ProcessCheckTask;

/**
 * Simple test for the Spring configuration boostrap.
 * 
 * @author oliver.burkhalter
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "config-test.xml")
public class MainTest {

    @Autowired
    private List<ProcessCheckTask> processCheckTaskList;

    @Test
    public void springConfigInitialization() {
	assertEquals(3, processCheckTaskList.size());
    }
}
