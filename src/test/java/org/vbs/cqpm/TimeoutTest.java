package org.vbs.cqpm;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.ScheduledFuture;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.vbs.cqpm.tasks.ProcessCheckTask;

/**
 * Tests the timeout functionality for a process check task in the Scheduler.
 * 
 * @author oliver.burkhalter
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "config-test.xml")
public class TimeoutTest {

    private static final Logger logger = LoggerFactory.getLogger(TimeoutTest.class);

    @Autowired
    private ThreadPoolTaskScheduler scheduler;

    @Autowired
    private List<ProcessCheckTask> processCheckTaskList;

    @Test
    public void testTimeoutProcessFailureCheck() {

	// Schedule the process check tasks
	for (ProcessCheckTask task : processCheckTaskList) {
	    ScheduledFuture<?> future = scheduler.schedule(task, new CronTrigger(task.getCron()));
	    task.setScheduledFuture(future);
	}

	try {
	    Thread.yield();
	    Thread.sleep(4000);
	} catch (InterruptedException e) {
	    logger.error("Thread error: " + e.getMessage());
	}

	// Test process check task status
	ProcessCheckTask task1 = processCheckTaskList.get(0);
	ScheduledFuture<?> future1 = task1.getScheduledFuture();
	ProcessCheckTask task2 = processCheckTaskList.get(1);
	ScheduledFuture<?> future2 = task2.getScheduledFuture();

	assertTrue("Scheduled task should be cancelled because of timeout.", future1.isCancelled());
	assertTrue("Scheduled task should be cancelled because of timeout.", future2.isCancelled());
    }

}
