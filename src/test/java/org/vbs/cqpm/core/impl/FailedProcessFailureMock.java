package org.vbs.cqpm.core.impl;

import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;

/**
 * This {@link ProcessFailurePattern} mock helps to test the "failed" status of
 * a process.
 * 
 * @author oliver.burkhalter
 */
public class FailedProcessFailureMock implements ProcessFailurePattern, Runnable {
    private boolean finished;
    private Process process;
    private Thread checkThread;

    public String getName() {
	return this.getClass().getName();
    }

    public void run() {
	// Just set process to failed
	process.setFailed(true);
    }

    public void start(Process process) {
	this.process = process;
	this.process.setFailed(false);
	finished = false;
	checkThread = new Thread(this);
	checkThread.start();
    }

    public void stop() {
	checkThread.interrupt();
    }

    public boolean finished() {
	return finished;
    }
}