package org.vbs.cqpm.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vbs.cqpm.core.Process;
import org.vbs.cqpm.core.ProcessFailurePattern;
import org.vbs.cqpm.tasks.ProcessCheckTask;

/**
 * This {@link ProcessFailurePattern} mock helps to test the overall thread
 * management for the process check tasks. This mock explicit simulates a too
 * long process failure check and forces a timeout which should be correctly
 * handled by the current {@link ProcessCheckTask} instance.
 * 
 * @author oliver.burkhalter
 */
public class TimeoutProcessFailureMock implements ProcessFailurePattern, Runnable {

    private static final Logger logger = LoggerFactory.getLogger(TimeoutProcessFailureMock.class);

    private boolean finished;

    private Thread checkThread;

    public String getName() {
	return this.getClass().getName();
    }

    public void run() {
	try {
	    // Timeout time
	    Thread.sleep(2000);
	} catch (InterruptedException e) {
	    logger.warn("TimeoutProcessFailureMock check has been interrupted: " + e.getMessage());
	}
    }

    public void start(Process process) {
	checkThread = new Thread(this);
	checkThread.start();
    }

    public void stop() {
	checkThread.interrupt();
    }

    public boolean finished() {
	return finished;
    }
}